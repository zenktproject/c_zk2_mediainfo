<?php
/*
* 公開・非公開ロジック
* 公開・非公開フィールド： public_private
*/
require_once("modules/Teams/Team.php");

class CustomLogicMediaInfo extends SugarBean
{
	//繰り返し処理防止
	private function changeMax($bean){
		$bean->max_logic_depth = 0;
	}
	
	public function custom_logic_mediainfo($bean, $event, $arguments)
	{
		$this->changeMax($bean);
		
		//フィールド存在チェック
		if(isset($bean->public_private_c)){
			//公開・非公開処理
			$this->changeTeamID($bean);
		}
		
		if(isset($bean->description)){
			//詳細内にあるURLにリンクタグを付ける
			$this->addUrlTag($bean);
		}
		
		$bean->save();
	}
	
	
	//公開・非公開処理
	public function changeTeamID($bean)
	{
		//作成中ならログインユーザーのプライベートチーム
		if($bean->public_private_c === 'creating'){
			$private_teamId = $GLOBALS['current_user']->getPrivateTeamID();
			
			$bean->team_id = $private_teamId;
			$teamSet = new TeamSet();
			$bean->team_set_id = $teamSet->addTeams(array($private_teamId));
			
		}
		//公開ならグローバルチーム
		if($bean->public_private_c === 'public'){
			$bean->team_id = 1;
			$bean->team_set_id = 1;
			//$bean->save();
		}
		
		//非公開ならログインユーザーの所属部署メンバーのプライベートチーム？
		//ロジックはまだ（仕様決定後）
		
	}
	
	//URL入力値にリンクタグを付ける
	public function addUrlTag($bean){
		$str = $bean->description;
		
		//まず<a></a>タグのみ除去
		$arep_str = preg_replace('@<a(?:>| [^>]*?>)(.*?)<\/a>@s', '$1', $str);
		//URLが含まれるパターン
		$pattern2 = '/((?:https?|ftp):\/\/[-_.!~*\'()a-zA-Z0-9;\/?:@&=+$,%#]+)/u';
		//パターンに一致する箇所に<a>タグを入れて整形
		$rep_str = preg_replace($pattern2, '<a href="\1" target="_blank">\1</a>', $arep_str);
		//上書き
		$bean->description = $rep_str;
		
	}
	
}