/*
 * custom - record.js ZK2_MediaInfo
 * メール送信ボタン押下⇒「送信しますか？y or n⇒もし、既に送信したことがあるレコードの場合「確認アラート」を出す
 * それでＯＫ押されて初めてAPI起動。実行中は「処理中・・・」などを表示する
 */
({
	extendsFrom: 'RecordView',		//modules/ZK2_MediaInfoのrecord.jsは無いので大元を継承?
	
	f_confirmMsg: '本当に送信しますか？',
	s_confirmMsg: 'に送信していますが、もう一度送信しますか？',
	
	
	initialize: function(options) {
		this._super('initialize', [options]);	//親のinitialize実行
		
		//メール送信ボタンクリック
		this.context.on('button:send_mail_btn:click', this.confirmAlert, this);
		
	},
	
	//メール送信
	sendMailApi: function(){
		var self = this;
		//var r_id = this.model.get('id');
		//var attach = this.model.get('attachment_c');
		
		
		var rssUrl = app.api.buildURL('ZK2_MediaInfo','sendMail',null,{ "record": self.model.get('id'), "attach": self.model.get('attachment_c')});
		app.api.call('create', rssUrl, null,{
			success: function (data) {
				if(data.status === "success"){
					app.alert.show('sendMailOK',
						{
							level: 'info',
							messages: data.message,
							autoClose: false
						}
					);
				}else{
					app.alert.show('sendMailerror',
						{
							level: 'warning',
							messages: data.message,
							autoClose: false
						}
					);
				}
				//modelをサーバーと同期させる
				self.model.fetch();
			},
			error: _.bind(function() {
				app.alert.show('systemError',
					{
						level: 'error',
						messages: '致命的なエラーが発生しました',
						autoClose: false
					}
				);
			}, this),
		});
		
		app.alert.show('sendMailInfo',
			{
				level: 'process',
				messages: '処理中です・・・',
				autoClose: true
			}
		);
		
/*
		var rssUrl = app.api.buildURL('ZK2_MediaInfo','sendMail',null,{ "record": self.model.get('id'), "attach": self.model.get('attachment_c')});
		app.api.call('create', rssUrl, null,{
			success: function (data) {
				if(data.status === "success"){
					app.alert.show('sendMailOK',
						{
							level: 'info',
							title: data.message,
							message: '送信OK',
							autoClose: false
						}
					);
				}else{
					app.alert.show('sendMailerror',
						{
							level: 'warning',
							title: data.message,
							message: 'アプリエラー',
							autoClose: false
						}
					);
				}
				//modelをサーバーと同期させる
				self.model.fetch();
			},
			error: _.bind(function() {
				app.alert.show('systemError',
					{
						level: 'error',
						title: '致命的なエラーが発生しました',
						message: 'apiエラー',
						autoClose: false
					}
				);
			}, this),
		});
		
		app.alert.show('sendMailInfo',
			{
				level: 'process',
				title: '処理中です・・・',
				message: '処理中',
				autoClose: true
			}
		);
*/
		
	},
	
	
	//第１メール送信確認アラート
	//フィールドチェック
	confirmAlert: function(){
		var self = this;
		var mail_to = this.model.get('send_mail_c');		//宛先
		var p_status = this.model.get('public_private_c');	//公開・非公開
		var attach = this.model.get('attachment_c');		//添付ファイル
		var msg = "";										//チェックエラー用メッセージ
		var f_msg = this.f_confirmMsg;						//アラート用メッセージ
		var err_flg = false;								//エラーフラグ
		
		//公開・非公開チェック
		if(p_status !== 'public'){
			//公開以外だったらエラー
			err_flg = true;
			msg = "ステータスが公開ではありません。";
		}
		
		//宛先存在チェック
		if(mail_to.length === 0){
			//宛先選択無し
			err_flg = true;
			if(msg !== ""){
				msg = msg + "<br>";
			}
			msg = msg + "宛先が選択されていません。";
		}
		
		//添付ファイルチェック
		if(attach === 'unattach'){
			//添付しない場合エラーではないがconfirm時に追加メッセージの表示
			f_msg = "ファイルを添付していません。<br>" + f_msg;
		}
		
		//エラーフラグチェック
		if(err_flg === false){
			//OKクリックで送信日時チェックへ
			app.alert.show('confirmSendMail',
				{
					level: 'confirmation',
					messages: f_msg,
					autoClose: false,
					onConfirm: function(){
						self.chkSendMailDate();
					},
				}
			);
		}else{
			//エラーアラート
			app.alert.show('confirmAlert_error',
				{
					level: 'warning',
					messages: msg,
					autoClose: true
				}
			);
		}
	},
	
	
	//送信日時チェック
	chkSendMailDate: function(){
		var self = this;
		var d_date = this.model.get('send_date_c');
		var dateformat = app.user.getPreference('datepref');
		var timeformat = app.user.getPreference('timepref');
		var datetime_format = dateformat + ' ' + timeformat;
		var alert_date = app.date(d_date).format(app.date.convertFormat(datetime_format));
		
		if(this.model.get('send_date_c')){
			//送信日時存在
			var s_msg = alert_date + this.s_confirmMsg;
			
			//OKクリックでメール送信APIへ
			app.alert.show('confirmSendMail',
				{
					level: 'confirmation',
					messages: s_msg,
					autoClose: false,
					onConfirm: function(){
						self.sendMailApi();
					},
				}
			);
		}else{
			//既にメール送信確認アラートOKクリック済なので
			//メール送信APIへ
			this.sendMailApi();
		}
	},
})
