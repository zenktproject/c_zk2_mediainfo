<?php
$module_name = 'ZK2_MediaInfo';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'record' => 
      array (
		//add start
		'buttons' =>
		array(
			array(
				'type' => 'button',
				'name' => 'cancel_button',
				'label' => 'LBL_CANCEL_BUTTON_LABEL',
				'css_class' => 'btn-invisible btn-link',
				'showOn' => 'edit',
			),
			array(
				'type' => 'rowaction',
				'event' => 'button:save_button:click',
				'name' => 'save_button',
				'label' => 'LBL_SAVE_BUTTON_LABEL',
				'css_class' => 'btn btn-primary',
				'showOn' => 'edit',
				'acl_action' => 'edit',
			),
			array(
				'type' => 'actiondropdown',
				'name' => 'main_dropdown',
				'primary' => true,
				'showOn' => 'view',
				'buttons' => array(
					array(
						'type' => 'rowaction',
						'event' => 'button:edit_button:click',
						'name' => 'edit_button',
						'label' => 'LBL_EDIT_BUTTON_LABEL',
						'acl_action' => 'edit',
					),
					array(
						'type' => 'rowaction',
						'event' => 'button:send_mail_btn:click',
						'name' => 'send_mail_btn',
						'label' => 'LBL_SEND_MAIL_BUTTON',
						'acl_action' => 'view',
					),
					array(
						'type' => 'shareaction',
						'name' => 'share',
						'label' => 'LBL_RECORD_SHARE_BUTTON',
						'acl_action' => 'view',
					),
					array(
						'type' => 'pdfaction',
						'name' => 'download-pdf',
						'label' => 'LBL_PDF_VIEW',
						'action' => 'download',
						'acl_action' => 'view',
					),
					array(
						'type' => 'pdfaction',
						'name' => 'email-pdf',
						'label' => 'LBL_PDF_EMAIL',
						'action' => 'email',
						'acl_action' => 'view',
					),
					array(
						'type' => 'divider',
					),
					array(
						'type' => 'rowaction',
						'event' => 'button:find_duplicates_button:click',
						'name' => 'find_duplicates_button',
						'label' => 'LBL_DUP_MERGE',
						'acl_action' => 'edit',
					),
					array(
						'type' => 'rowaction',
						'event' => 'button:duplicate_button:click',
						'name' => 'duplicate_button',
						'label' => 'LBL_DUPLICATE_BUTTON_LABEL',
						'acl_module' => 'ZK2_MediaInfo',
						'acl_action' => 'create',
					),
					array(
						'type' => 'rowaction',
						'event' => 'button:audit_button:click',
						'name' => 'audit_button',
						'label' => 'LNK_VIEW_CHANGE_LOG',
						'acl_action' => 'view',
					),
					array(
						'type' => 'divider',
					),
					array(
						'type' => 'rowaction',
						'event' => 'button:delete_button:click',
						'name' => 'delete_button',
						'label' => 'LBL_DELETE_BUTTON_LABEL',
						'acl_action' => 'delete',
					),
				),
			),
			array(
				'name' => 'sidebar_toggle',
				'type' => 'sidebartoggle',
			),
		),
		//add end
        'panels' => 
        array (
          0 => 
          array (
            'name' => 'panel_header',
            'label' => 'LBL_RECORD_HEADER',
            'header' => true,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'picture',
                'type' => 'avatar',
                'width' => 42,
                'height' => 42,
                'dismiss_label' => true,
                'readonly' => true,
              ),
              1 => 'name',
              2 => 
              array (
                'name' => 'favorite',
                'label' => 'LBL_FAVORITE',
                'type' => 'favorite',
                'readonly' => true,
                'dismiss_label' => true,
              ),
              3 => 
              array (
                'name' => 'follow',
                'label' => 'LBL_FOLLOW',
                'type' => 'follow',
                'readonly' => true,
                'dismiss_label' => true,
              ),
            ),
          ),
          1 => 
          array (
            'name' => 'panel_body',
            'label' => 'LBL_RECORD_BODY',
            'columns' => 2,
            'labelsOnTop' => true,
            'placeholders' => true,
            'newTab' => false,
            'panelDefault' => 'expanded',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'zk2_media_zk2_mediainfo_name',
              ),
              1 => 
              array (
                'name' => 'important',
                'studio' => 'visible',
                'label' => 'LBL_IMPORTANT',
              ),
              2 => 
              array (
                'name' => 'description',
                'span' => 12,
                'type' => 'htmleditable_tinymce',
                'tinyConfig' => 
                array (
                  'script_url' => 'include/javascript/tiny_mce/tiny_mce.js',
                  'height' => '200px',
                  'width' => '100%',
                  'theme' => 'advanced',
                  'skin' => 'sugar7',
                  'plugins' => 'style,paste,inlinepopups,pagebreak',
                  'entity_encoding' => 'raw',
                  'forced_root_block' => false,
                  'theme_advanced_buttons1' => 'code,separator,bold,italic,underline,strikethrough,separator,bullist,numlist,separator,justifyleft,justifycenter,justifyright,justifyfull,separator,forecolor,backcolor,separator,fontsizeselect,link,unlink',
                  'theme_advanced_toolbar_location' => 'top',
                  'theme_advanced_toolbar_align' => 'left',
                  'theme_advanced_statusbar_location' => 'none',
                  'theme_advanced_resizing' => false,
                  'schema' => 'html5',
                  'template_external_list_url' => 'lists/template_list.js',
                  'external_link_list_url' => 'lists/link_list.js',
                  'external_image_list_url' => 'lists/image_list.js',
                  'media_external_list_url' => 'lists/media_list.js',
                  'theme_advanced_path' => false,
                  'theme_advanced_source_editor_width' => 500,
                  'theme_advanced_source_editor_height' => 400,
                  'inlinepopups_skin' => 'sugar7modal',
                  'relative_urls' => false,
                  'remove_script_host' => false,
                ),
              ),
			  //add
			  array(
				'name' => 'public_private_c',
				'studio' => 'visible',
				'label' => 'LBL_PUBLIC_PRIVATE',
			  ),
			  array(
				'name' => 'send_mail_c',
				'studio' => 'visible',
				'label' => 'LBL_SEND_MAIL',
			  ),
			  array(
				'name' => 'send_date_c',
				'readonly' => true,
				'label' => 'LBL_SEND_DATE',
			  ),
			  //add end
            ),
          ),
          2 => 
          array (
            'name' => 'panel_hidden',
            'label' => 'LBL_SHOW_MORE',
            'hide' => true,
            'columns' => 2,
            'labelsOnTop' => true,
            'placeholders' => true,
            'newTab' => false,
            'panelDefault' => 'expanded',
            'fields' => 
            array (
              0 => 'assigned_user_name',
              1 => 'team_name',
              2 => 'date_modified',
              3 => 'date_entered',
              4 => 
              array (
                'name' => 'modified_by_name',
                'readonly' => true,
                'label' => 'LBL_MODIFIED',
              ),
              5 => 
              array (
                'name' => 'created_by_name',
                'readonly' => true,
                'label' => 'LBL_CREATED',
              ),
            ),
          ),
        ),
        'templateMeta' => 
        array (
          'useTabs' => false,
        ),
      ),
    ),
  ),
);
