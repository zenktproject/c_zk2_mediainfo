<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('data/BeanFactory.php');
require_once('include/api/SugarApi.php');
require_once('modules/Emails/Email.php');
require_once('include/SugarPHPMailer.php');
require_once('vendor/PHPMailer/class.phpmailer.php');
require_once('modules/ZK2_MediaInfo/ZK2_MediaInfo.php');

class SendMailApi extends SugarApi
{
	
	private $uploadPath = "upload/";	//sugarアップロードディレクトリパス
	private $encoding = "base64";		//添付ファイル追加処理の引数
	private $chkByte = 9000000;			//添付ファイル合計サイズチェック用：9MB
	
	public function registerApiRest()
	{
		return array(
			'sendMail' => array(
				//request type
				'reqType' => 'POST',
				//endpoint path
				'path' => array('ZK2_MediaInfo', 'sendMail'),
				//endpoint variables
				'pathVars' => array(''),
				//method to call
				'method' => 'sendMail',
				//short help string to be displayed in the help documentation
				'shortHelp' => '',
				//long help to be displayed in the help documentation
				'longHelp' => '',
			),
		);
	}
	
	//情報メール送信
	public function sendMail($api, $args)
	{
		$err_flg = false;				//エラーフラグ
		$beanId = $args['record'];		//jsからのパラメータ
		$attach_flg = $args['attach'];	//jsからのパラメータ
		
		//レコードID存在チェック
		if(!isset($beanId)){
			$status = "chkerror";
			$message = "パラメータが不正です。";
		}else{
			//情報モジュールインスタンス
			$bean = BeanFactory::getBean('ZK2_MediaInfo', $args['record']);
			
			//宛先選択チェック
			if($bean->send_mail_c == ""){
				$err_flg = true;
				$status = "chkerror";
				$message = "宛先が選択されていません。";
			}
			
			//エラーフラグ無しでメール送信ロジックへ
			//-------------------------メールデータ作成-------------------------//
			if($err_flg != true){
				//---------------宛先メールアドレス抽出---------------//
				$mail_to_address = array();
				$mail_to_address = $this->getMailAddr($bean);
				$mail_to_count = count($mail_to_address);
				//---------------宛先メールアドレス抽出---------------//
				
				$emailObj = new Email();						//メールインスタンス
				$defaults = $emailObj->getSystemDefaultEmail();	//メール送信設定の名前とアドレスを取得
				$mail = new SugarPHPMailer();
				$mail->setMailerForSystem();
				$mail->From = $defaults['email'];				//差出人：メールアドレス
				$mail->FromName = $defaults['name'];			//差出人：名前
				$important = $GLOBALS['app_list_strings']['zk2_mediainfo_priority_list'][$bean->important];//重要度のラベル取得（件名に追加）
				$baitai = $GLOBALS['app_list_strings']['media_list'][$bean->media_c];					   //媒体のラベル取得（件名に追加）
				$mail->Subject = from_html("【".$important."】【".$baitai."情報】".$bean->name);		   //件名
				$mail->IsHTML();								//メール本文をhtml形式に変換
				$header = preg_replace('/(\r\n|\r|\n)/ ', '<br/>', $bean->header_c);					   //改行をHTMLのタグに置換
				$footer = preg_replace('/(\r\n|\r|\n)/ ', '<br/>', $bean->footer_c);					   //改行をHTMLのタグに置換
				$text = $bean->description;
				$body = $header."<br/><br/>".$text."<br/><br/>".$footer;  //順番に本文を整形
				$mail->Body = $body;							//メール本文
				//$plain = preg_replace('(<br/>|<br />)', '\n', $body);
				//$mail->AltBody = $bean->footer_c;				//プレインテキストでも読めるように（調査中）
				$mail->prepForOutbound();
				
				//---------------宛先追加---------------//
				for($i = 0; $i < $mail_to_count; $i++){
					$ret_addAdr = $mail->AddAddress($mail_to_address[$i]);
					if($ret_addAdr === false){
						//アドレス追加エラー
						$err_flg = true;
						$status = "chkerror";
						$message = "予期しないエラー：宛先追加エラー";
					}
				}
				//---------------宛先追加---------------//
				
				//---------------添付ファイル追加---------------// 
				if($attach_flg !== 'unattach'){
					//データビューに紐づいてる添付ファイルをメールに添付
					$file_id = array();
					$obj_zk2mi = new ZK2_MediaInfo();
					$obj_zk2mi->retrieve($bean->id);
					$obj_zk2mi->load_relationship('zk2_mediainfo_notes_1');
					foreach($obj_zk2mi->zk2_mediainfo_notes_1->getBeans() as $rec_note){
						array_push($file_id, $rec_note->id);
						$path = $this->uploadPath.$rec_note->id;
						$filename = $rec_note->filename;
						$type = $rec_note->file_mime_type;
						if(isset($path, $filename, $type)){
							//メールにファイル添付
							$mail->AddAttachment($path, $filename, $this->encoding, $type);
							
						}
					}
					
					//ファイルサイズ計算
					$total_fileSize = 0;	//一応初期化
					$file_id_count = count($file_id);
					for($k = 0; $k < $file_id_count; $k++){
						$filePath = $this->uploadPath.$file_id[$k];		//アップロードファイルパス
						$s_fileSize = filesize($filePath);
						$total_fileSize = $total_fileSize + $s_fileSize;
					}
					//ファイスサイズチェック
					if($total_fileSize >= $this->chkByte){
						$err_flg = true;
						$status = "chkerror";
						$message = "添付ファイルサイズの合計が10MBを超えています。";
						//$GLOBALS['log']->fatal($total_fileSize);
						//$GLOBALS['log']->fatal($megaSize);
					}
				}
				//---------------添付ファイル追加---------------//
				
				//---------------メール送信---------------//
				if($err_flg != true){
					$res = @$mail->Send();
					if($res === false){
						//送信エラー
						$status = "chkerror";
						$message = "予期しないエラー：メールの送信に失敗しました";
					}else{
						$status = "success";
						$message = "メールを送信しました。";
						//情報レコード送信日時更新
						$this->updateSendDate($bean);
					}
				}
				//---------------メール送信---------------//
			}
		}
		
		return array("message" => $message, "status" => $status);
	}
	
	
	//宛先メールアドレス取得
	public function getMailAddr($bean){
		$mail_to = $bean->send_mail_c;							//例： ^01^,^02^,^03^
		$mail_to_key = array();									//配列初期化
		$rep_mail_to = preg_replace('/\^/', '', $mail_to);		//^文字列^,^文字列^⇒文字列,文字列
		$mail_to_key = explode(",", $rep_mail_to);				//配列変換
		$mail_to_count = count($mail_to_key);					//配列要素数
		
		$mail_to_address = array();								//メールアドレス格納用配列
		//画面で選択された宛先からメールアドレスを取得
		//メールアドレス格納ドロップダウンリスト：send_mail_address_list
		for($i = 0; $i < $mail_to_count; $i++){
			$mail_to_id = $mail_to_key[$i];
			$address_list = $GLOBALS['app_list_strings']['send_mail_address_list'];
			$mail_to_address[$i] = $address_list[$mail_to_id];
		}
		
		return $mail_to_address;
	}
	
	
	//SQLで対象レコードの送信日時を更新する
	//bean更新だとロジックフックが走るため
	public function updateSendDate($bean){
		$db = $GLOBALS['db'];
		$nowDateTime = $GLOBALS['timedate']->nowDb();
		$beanID = $bean->id;
		
		//SQL整形
		$update_sql = sprintf("update zk2_mediainfo_cstm SET send_date_c = '%s' WHERE id_c = '%s'", $nowDateTime, $beanID);
		
		//SQL実行
		$db->query($update_sql);
	}
	
}
 
?>